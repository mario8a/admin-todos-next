import { cookies } from 'next/headers'
import { TabBar } from "@/app/components/TabBar";

export const metadata = {
  title: 'Cookies Page',
  description: 'SEO title',
}

export default function CookiesPage() {

  const cookieStore = cookies()
  const cookieTab = cookieStore.get('selectedTab')?.value ?? '1';



  return (
    <div className="grid grid-cols-2 sm:grid-cols-2 gap-3">
      <span className="text-3xl">Tabs</span>

      <div className="flex flex-col">
        <TabBar
          currentTab={+cookieTab}
        />
      </div>

    </div>
  );
}